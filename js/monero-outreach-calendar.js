var ico_b = '<div class="MOCalIco"><div class="MOCalBirthday"><svg viewBox="0 0 16 51"><path opacity=".4" d="M3 11c0 4 2 7 5 7s6-3 6-7S8 0 8 0 3 7 3 11z"/><path d="M8 24l6-3H5c-4 2-5 6-5 6v5c0-1 1-6 8-8zm0 5c-7 2-8 7-8 8v4s1-5 8-7 8-6 8-7v-4c0 1-2 4-8 6zm0 10c-7 2-8 7-8 7v4c1-2 3-5 8-7 7-1 8-6 8-7v-4c0 1-1 5-8 7zm0 9l-5 3h10c3-2 3-4 3-5v-5c0 1-1 5-8 7zM5 14c0 2 1 4 3 4s4-2 4-4c0-3-4-7-4-7s-3 4-3 7z"/></svg></div></div>',
	$MOCal = {
		'lbl':{
			1:{'2':ico_b+'<span class="opt">Isaac </span>Asimov','4':''+ico_b+'<span class="opt">Isaac </span>Newton','25':ico_b+'<span class="opt">Robert </span>Boyle'},
			2:{'15':ico_b+'Galileo<span class="opt"> Galilei</span>','19':ico_b+'<span class="opt">Nicolaus </span>Copernicus','29':'Leap Day'},
			3:{'14':ico_b+'<span class="opt">Albert </span>Einstein'},
			4:{'18':ico_b+'Monero','25':ico_b+'<span class="opt">Guglielmo </span>Marconi','30':ico_b+'<span class="opt">Carl </span>Gauss'},
			5:{'18':ico_b+'<span class="opt">Oliver </span>Heaviside','23':ico_b+'<span class="opt">Carl </span>Linnaeus'},
			6:{'13':'<span class="opt">James Clerk </span>Maxwell','23':ico_b+'<span class="opt">Alan </span>Turing'},
			7:{'10':ico_b+'<span class="opt">Nikola </span>Tesla','12':ico_b+'Buckminster<span class="opt"> Fuller</span>'},
			8:{'1':ico_b+'<span class="opt">Maria </span>Mitchell','12':ico_b+'<span class="opt">Erwin </span>Schrödinger','29':ico_b+'<span class="opt">John </span>Locke'},
			9:{'18':ico_b+'<span class="opt">Léon </span>Foucault','22':ico_b+'<span class="opt">Michael </span>Faraday'},
			10:{'7':ico_b+'Niels Bohr','17':ico_b+'CryptoNote'},
			11:{'7':ico_b+'Curie','20':ico_b+'Mandelbrot','23':'Fibonacci Day'},
			12:{'10':ico_b+'Lovelace','15':ico_b+'<span class="opt">L. L. </span>Zamenhof','26':'<span class="opt">Charles </span>Babbage'}
		},
		'clr':{
			1:'bbdfc3',
			2:'e47ed9',
			3:'b7bdb0',
			4:'1d5c9e',
			5:'5a8870',
			6:'182c3e',
			7:'9ab5b6',
			8:'c6f6f9',
			9:'171217',
			10:'0b1923',
			11:'603141',
			12:'b8ae5c',
			13:'89523f',
			14:'0f262c',
			15:'29313a',
			16:'292427',
			17:'b9b7bd',
			18:'d9dad8',
			19:'b9d2df',
			20:'b2afac',
			21:'dbcfb8',
			22:'c4cbca',
			23:'e1d7d7',
			24:'050004',
			25:'7a7f85',
			26:'b6c0db',
			27:'ece5e5',
			28:'eef5f5',
			29:'a1beb8',
			30:'594645',
			31:'2d2626'
		}
	};

function MOCalendar(){
	var d = new Date(),
		dy = ('0'+d.getDate()).slice(-2),
		mo = d.getMonth() + 1,
		yr = d.getFullYear(),
		flt = '',
		abbr = 'n',
		res = (VPortW < 800) ? '_800' : '';
		
	if(VPortW < 700){
		$CAL['mde'] = 'list';
		abbr = y;
	}
	
	setAttr(getElem('MastArt'), {'src':$ENV['cdn']+'img/monero-outreach/Calendar/'+dy+res+'.jpg', 'img-dna':dy+res+'|jpg|1600|400|'+$MOCal['clr'][dy], 'alt':'Monero Calendar: '+dy});
	
	var mo_sel = '',
		yr_sel = '<option>2019</option><option>2020</option><option>2021</option><option>2022</option>';

	for(var m = 1; m <= 12; m++){
		mo_sel += '<option value="'+m+'">'+LocalizeDayMonth(m, 'm', lng, abbr)+'</option>';
	}

	getElem('MOCalNav').innerHTML = '<div id="MOCalUTC" class="Btn32 Switch">'+$ICO['switch']+'</div><div id="MOCalUTCLbl" class="txt_tiny o5 noselect">UTC</div>'+
		'<select id="MOCalMonth" class="CalMenuMonth CalMenuSelect">'+mo_sel+'</select>'+
		'<select id="MOCalYear" class="CalMenuYear CalMenuSelect">'+yr_sel+'</select>'+
		'<select id="MOCalFilter" class="CalMenuSelect"><option value="">All</option><option value="Events">Events</option><option value="Meetings">Meetings</option></select>'+
		'<div id="CalLoading" class="Ico24 C1fl"></div>';
		
	Switch('F', 'MOCalUTC');
	LoadingIcon('#CalLoading');
	setVal('MOCalMonth', mo);
	setVal('MOCalYear', yr);

	load_MOCalendar(mo, yr, flt);
}

function load_MOCalendar(mo, yr, flt){
	var NumDays = daysInMonth(mo, yr),
		dtes = new Date(yr+'-'+mo+'-01 00:00:00'),
		dtee = new Date(yr+'-'+mo+'-'+NumDays+' 23:59:59');
		
	EmptyDiv('CalLoading');

	LoadingIcon('#CalLoading');
	$CAL['data'] = {};
	$CAL['data_days'] = {};
	get_Data('query_Content', 'Content', {'loc':'US', 'lng':'en', 'typ':'e', 'dtes':dtes.getTime() / 1000, 'dtee':dtee.getTime() / 1000, 'odir':'A'}, function(d){
		if(d && d['data']) $CAL['data'] = d['data'];
		draw_MOCalendar();
	});
}

function prepdata_MOCalendar(){
	var flt = getVal('MOCalFilter'),
		utc = (getElem('MOCalUTC').getAttribute('data-switch') === 'T') ? 'T' : 'F',
		utc_offset = 0,
		r = {},
		i = 0;
		
	if(utc === 'T'){
		var u = new Date();
		utc_offset = u.getTimezoneOffset() * 60;	
	}
	
	for(var k in $CAL['data']){
		var v = $CAL['data'][k];
		if(flt === '' || (v['idx0'] && v['idx0'] === flt)){
			var p = new Date(0),
				t = (utc === 'T') ? parseInt(v['pub_start']) + utc_offset : v['pub_start'];
				
			p.setUTCSeconds(t);
			var dy = p.getDate();
			
			if(!$CAL['data_days'][dy]) $CAL['data_days'][dy] = {};
			$CAL['data_days'][dy][i] = {'key':k, 'tme':t};
			i++;
		}
	}
}

function draw_MOCalendar(){
	var vico = ($CAL['mde'] === 'list') ? 'btn_calview' : 'btn_callist',
		tme_mde = (getElem('MOCalUTC').getAttribute('data-switch') === 'T') ? 'utc_tme' : 'tme',
		mo = parseInt(getVal('MOCalMonth')),
		yr = parseInt(getVal('MOCalYear')),
		ins = '';
			
	LoadingIconDone('CalLoading', '<div id="MOCalSwitchView" data-view="'+$CAL['mde']+'">'+$ICO[vico]+'</div>');
	prepdata_MOCalendar();
		
	if($CAL['mde'] === 'list'){
		var num = isObj($CAL['data_days']),
			hlf = (num > 1) ? Math.floor(num / 2) : 0,
			i = 0;

		if(num === 0){
			ins += '<p class="shimtop50 center">No Results</p>';
		}else{
			ins += '<div class="Col2 Magic700"><div class="LR98">';
			for(var day in $CAL['data_days']){
				var daytme = new Date(Date.UTC(yr, mo - 1, day, '01', '01', '01')).getTime() / 1000,
					daylable = ($MOCal['lbl'][mo][day]) ? $MOCal['lbl'][mo][day] : '',
					dayabbr = (VPortW <= 700) ? 'y' : 'n',
					daynme = LocalizeDayMonth(prepDate(daytme, 'day'), 'd', $CAL['lng'], dayabbr);

				if(i === hlf) ins += '</div></div><div class="Col2 Magic700"><div class="LR98">';
				
				ins += '<div class="CalListDay">'+
					'<div class="CalDay__Num txt_copytext">'+day+' &middot; '+daynme+'</div>'+
					'<div class="CalDay__Lbl txt_tiny">'+daylable+'</div>'+
					'<div class="LR95 hbar o8"></div>';
				
				for(var k in $CAL['data_days'][day]){
					var v = $CAL['data_days'][day][k],
						key = v['key'],
						d = $CAL['data'][key],
						$U = prepURL(d),
						$THM = prepIMG(d['thumb']),
						et = (d['idx0']) ? ' data-typ="'+d['idx0']+'"' : '',
						title = d['title'],
						thumb = '',
						idna = ($U['div']['classname'] === 'LinkPop') ? ' img-dna="'+d['thumb']+'"' : '';
	
					if($U && $U['url'] && $U['url'] != ''){
						title = '<a '+$U['a']['full']+idna+'><span class="C3">'+d['title']+'</span></a>';
					}
					if(d['caption']){
						title += '<div class="txt_lbl shim5">'+d['caption']+'</div>';
					}
					if($THM){
						var thumbclr = ($THM['color']) ? $THM['color'] : '#eaeaea',
						thumb = '<div class="DivThumb LinkPop" style="background-color:'+thumbclr+';background-image:url('+$ENV['cdn']+$THM['min']+')"'+idna+'></div>';
					}
					
					ins += '<div class="CalListEvt txt_smalltext">'+
						'<div class="CalListTime">'+prepDate(v['tme'], tme_mde)+'<div class="CalListDownload"><a href="http://calendar.monerooutreach.org/'+d['xid']+'.ics" class="txt_tiny" target="_blank">Download .ics</a></div></div>'+
						'<div class="CalListThumb">'+thumb+'</div>'+
						'<div class="CalListDesc">'+title+'</div>'+
					'</div>';
				}
				ins += '</div>';
				i++;
			}
			ins += '</div></div>';
		}
		getElem('MoneroCalendar').innerHTML = '<div id="MOCalList">'+ins+'</div>'
	}else{
		DrawCalendarGrid('MoneroCalendar', mo, yr, $MOCal['lbl']);
		for(var day in $CAL['data_days']){
			for(var k in $CAL['data_days'][day]){
				var v = $CAL['data_days'][day][k],
					key = v['key'],
					d = $CAL['data'][key],
					$U = prepURL(d),
					$THM = prepIMG(d['thumb']),
					et = (d['idx0']) ? ' data-typ="'+d['idx0']+'"' : '',
					title = d['title'],
					thumb = '',
					idna = ($U['div']['classname'] === 'LinkPop') ? ' img-dna="'+d['thumb']+'"' : '';

				if($U && $U['url'] && $U['url'] != ''){
					title = '<a '+$U['a']['full']+idna+'><span class="C3">'+d['title']+'</span></a>';
				}
				if($THM){
					var thumbclr = ($THM['color']) ? $THM['color'] : '#eaeaea',
					thumb = '<div class="DivThumb LinkPop" style="background-color:'+thumbclr+';background-image:url('+$ENV['cdn']+$THM['min']+')"'+idna+'></div>';
				}

				ins += '<div class="CalDay__Events__Event txt_lbl"'+et+'>'+
					'<div class="CalDay__Events__EventLbl">'+prepDate(v['tme'], tme_mde)+' '+thumb+title+'</div>'+
				'</div>';
			}
			getElem('MoneroCalendar_'+day).querySelector('.CalDay__Events').innerHTML = ins;
		}
	}
}

function dayview_MOCalendar(tar, day){
	var con = '',
		tme_mde = ($('#MOCalUTC').attr('data-switch') === 'T') ? 'utc_tme' : 'tme';

	if($CAL['data_days'][day]){
		for(var k in $CAL['data_days'][day]){
			var v = $CAL['data_days'][day][k],
				key = v['key'],
				d = $CAL['data'][key],
				$U = prepURL(d),
				$THM = prepIMG(d['thumb']),
				et = (d['idx0']) ? ' data-typ="'+d['idx0']+'"' : '',
				title = d['title'],
				thumb = '',
				idna = ($U['div']['classname'] === 'LinkPop') ? ' img-dna="'+d['thumb']+'"' : '';
				
			if($THM){
				var thumbclr = ($THM['color']) ? $THM['color'] : '#eaeaea',
				thumb = '<div class="DivThumb LinkPop" style="background-color:'+thumbclr+';background-image:url('+$ENV['cdn']+$THM['min']+')"'+idna+'></div>';
			}

			if($U && $U['url'] && $U['url'] != ''){
				title = '<a '+$U['a']['full']+idna+'><span class="C3">'+d['title']+'</span></a>';
			}
			if(d['caption']){
				title += '<div class="txt_lbl shim5">'+d['caption']+'</div>';
			}
			
			con += '<div class="CalListDay">'+
				'<div class="CalListTime">'+prepDate(v['tme'], tme_mde)+'<div class="CalListDownload"><a href="http://calendar.monerooutreach.org/'+d['xid']+'.ics" class="txt_tiny" target="_blank">Download .ics</a></div></div>'+
				'<div class="CalListThumb">'+thumb+'</div>'+
				'<div class="CalListDesc">'+title+'</div>'+
			'</div>';
		}
	}else{
		con += '<div class="shimtop50 center o7">'+$LNG['ui'][$CAL['lng']]['noresults']+'</div>';
	}
	
	var daytme = new Date(parseInt(getVal('MOCalYear')), parseInt(getVal('MOCalMonth')) - 1, day, 0, 0, 0).getTime() / 1000,
		dayabbr = (VPortW <= 700) ? 'y' : 'n',
		daynme = LocalizeDayMonth(prepDate(daytme, 'day'), 'd', $CAL['lng'], dayabbr),
		hdr = '<div class="CalDay--Expanded__Lbl txt_copytext"><b>'+prepDate(daytme, 'dte')+' '+daynme+'</b></div>'+
			'<div class="CalDay--Expanded__Close Btn32 C1fl C2fl_hov rot45">'+$ICO['plus']+'</div>';
	
	getElem(tar).innerHTML += '<div class="CalDay__Expanded C1br txt_copytext MOShadow">'+hdr+con+'</div>';
}

/*! MO Calendar Events */
document.addEventListener('change', f => {
	var e = f.target,
		id = e.getAttribute('id'),
		c = e.classList;
	
	if(c.contains('CalMenuSelect')){
		if(id === 'MOCalFilter'){
			$CAL['data_days'] = {};
			draw_MOCalendar();
		}else{
			load_MOCalendar(getVal('MOCalMonth'), getVal('MOCalYear'), getVal('MOCalFilter'));
		}
	}
});

document.addEventListener('click', f => {
	var e = f.target,
		id = e.getAttribute('id');

	if(id === 'MOCalSwitchView'){
		$CAL['mde'] = ($CAL['mde'] === 'list') ? 'cal' : 'list';
		draw_MOCalendar();
	}else if(id === 'MOCalUTC'){
		var nvl = (e.getAttribute('data-switch') === 'T') ? 'F' : 'T';
		Switch(nvl, e);
		$CAL['data_days'] = {};
		draw_MOCalendar();
	}else{
		var c = e.classList;
		if(c.contains('CalDay')){
			byClass('CalDay', 'rem', 'CalDay--Expanded');
			byClass('CalDay__Expanded', 'del');
			e.classList.add('CalDay--Expanded');
			var io = id.split('_');
			dayview_MOCalendar(io[0], io[1]);
		}else if(c.contains('CalDay--Expanded__Close')){
			byClass('CalDay', 'rem', 'CalDay--Expanded');
			byClass('CalDay__Expanded', 'del');
		}
	}
});