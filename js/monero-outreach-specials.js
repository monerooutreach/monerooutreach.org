var monerologo = '<svg viewBox="0 0 372.87 99"><path fill="#F16924" d="M49.5 0c-31.05 0-58 30.02-47 65.13h14.82V23.48L49.5 55.66l32.18-32.17v41.64h14.81A49.48 49.48 0 0049.5 0V0z"/><path fill="#4D4D4D" d="M256.3 27.5h28.5v8.2h-20.17l-.03 9h20.2v8h-20.2v10.5h20.2v8.3h-28.5v-44zm-42.7 43.92l8.4.08V42.6l18.8 28.9 8-.1V27.5h-8.4v28.9l-18.8-28.9h-8v43.92zm-101.2.08h8.1l4.9-27.8 9.1 27.8h7.4l9.4-27.8 4.7 27.8h8l-7.3-44h-8.1l-10.3 30.6-10.1-30.6H120l-7.6 44zm97-22c0 12.12-9.78 21.9-21.9 21.9s-22-9.78-22-21.9 9.88-22 22-22 21.9 9.88 21.9 22zm-8.5 0c0-7.4-6-13.5-13.4-13.5S174 42.1 174 49.5s6.1 13.4 13.5 13.4 13.4-6 13.4-13.4zm126.2 22H318l-9.9-18.67-7.3-.03v18.7h-8.35v-44h20.05c13.38 0 18.07 18.85 4.3 24.5l10.3 19.5zM317 40.2c0-2.48-2.06-4.5-4.54-4.5H300.8v8.98l11.7.02a4.5 4.5 0 004.5-4.5zm55.87 9.3a21.92 21.92 0 01-21.95 21.92c-12.12 0-21.92-9.8-21.92-21.92s9.78-21.99 21.9-21.99a22 22 0 0121.97 21.99zm-8.57 0a13.4 13.4 0 00-13.38-13.45c-7.4 0-13.42 6.04-13.42 13.45 0 7.4 6.1 13.4 13.5 13.4s13.3-6 13.3-13.4zm-336.24-.49v26.2H7.19a49.52 49.52 0 0084.62 0H70.94v-26.2L49.5 70.45 28.06 49z"/><path fill="#FFF" d="M81.68 65.1V23.5L49.5 55.66 17.32 23.5v41.6H2.52A49.41 49.41 0 007.2 75.2h20.86V49.01L49.5 70.45 70.94 49V75.2H91.8a49.4 49.4 0 004.67-10.1h-14.8z"/></svg>';

function Special_Benefit(){
	RemoveDiv('MastArt');
	var l2 = getElem('LayerData').getElementsByTagName('li'),
		len = l2.length,
		kmh = getElem('KnowHeadText').innerHTML,
		ins = '<div class="LR95">'+
			'<div id="BenefitLogo" class="Col3">'+monerologo+'</div><div id="BenefitHeadline" class="Col3-2 center">'+kmh+'</div>'+
		'</div>'+
		'<div id="BenefitBody">';

	for(var i = 0; i < len; i++){
		ins += '<div class="Col3"><div class="BenefitsInner"><div id="BS'+i+'" class="BenefitSpriteBox"><div class="BenefitSprite" style="left:-'+(i * 100)+'%"></div></div>'+l2[i].innerHTML+'</div></div>';
	}

	ins += '</div>';

	getElem('header').querySelector('.RInner').innerHTML += '<div id="Benefits">'+ins+'</div>';
	
}
function Special_Space(){
	var e = getElem('MastArt'),
		ins = '';

	ins += Special_SpaceRocket('3', 'Community');
	ins += Special_SpaceRocket('2', 'Technology');
	ins += Special_SpaceRocket('1', 'Privacy');

	e.parentNode.innerHTML += '<input type="hidden" id="SpaceIdx" value="3" /><div id="Space"><div id="SpaceText">'+ins+'</div></div>';

	/*! Space Events */
	document.addEventListener('click', f => {
		var e = f.target,
			c = e.classList;
		
		if(c.contains('SpaceNext')){
			Special_SpaceNext(e.getAttribute('data-next'))
		}
	});

	document.onkeypress = function(e){
		e = e || window.event;
		console.log(e.keyCode);
		if(e.keyCode === 110 || e.keyCode === 78){
			Special_SpaceNext(getVal('SpaceIdx'));
		}
	};
}

function Special_SpaceRocket(idx, title){
	var ins = '<div class="SpaceGroup">'+
	'.<br>|<br>&nbsp;\\<br>/ &nbsp;\\<br>/ &nbsp; &nbsp;\\<br>'+
	'| &nbsp;'+idx+'&nbsp; |<br>| &nbsp; &nbsp; |<br>MONERO<br>/ &nbsp; &nbsp;&nbsp; \\<br>'+title+'<br>'+
	'--------------<br><br>'+
	'<div class="SpaceStage1">';

	var l2 = getElem('LayerData'+idx).getElementsByTagName('li'),
		len = l2.length;

	for(var i = 0; i < len; i++){
		ins += l2[i].innerHTML+'<br><br>';
		if(i === 1){
			ins += '</div>-------------------------<br><br>'+
				'<div class="SpaceStage2">';
		}else if(i === 3){
			ins += '</div>----------------------------------<br><br>';
		}
	}

	ins += '---------------------------------------<br>'+
	'<div class="SpaceNext" data-next="'+idx+'">'+
	'/| &nbsp; <b>[N]</b> &nbsp; |\\<br>'+
	'/ | &nbsp; &nbsp;<b>E</b>&nbsp; &nbsp; | \\<br>'+
	'/ &nbsp;| &nbsp; &nbsp;<b>X</b>&nbsp; &nbsp; | &nbsp;\\<br>'+
	'/ &nbsp; / &nbsp; &nbsp;<b>T</b>&nbsp; &nbsp; \\ &nbsp; \\<br>'+
	'/ &nbsp; / &nbsp; ^ &nbsp; ^ &nbsp; \\ &nbsp; \\<br>';
	if(idx == '3'){
		ins += '/ &nbsp; / &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \\ &nbsp; \\';
	}else if(idx == '2'){
		ins += '/ &nbsp; / &nbsp; &nbsp;(&nbsp; &nbsp;)&nbsp; &nbsp; \\ &nbsp; \\<br>'+
			'( &nbsp; &nbsp; ';
	}else if(idx == '1'){
		ins += '/ &nbsp; / &nbsp; &nbsp;(&nbsp; &nbsp;)&nbsp; &nbsp; \\ &nbsp; \\<br>'+
			'(&nbsp;|&nbsp;)<br>'+
			'((&nbsp;|&nbsp;))<br>'+
			'((&nbsp; | &nbsp;))<br>'+
			'((&nbsp; : &nbsp;))<br>'+
			'((&nbsp; &nbsp;))<br>'+
			'(&nbsp;&nbsp;)<br>';
			':<br>:';
	}
	
	ins += '</div></div>';

	return ins;
}
function Special_SpaceNext(idx){

	var e = getElem('SpaceText'),
		ndx = '2';

	if(idx == '1'){
		e.style.marginTop = '0';
		ndx = '3';
	}else if(idx == '2'){
		e.style.marginTop = '-400%';
		ndx = '1';
	}else{
		e.style.marginTop = '-200%';
	}
	console.log(idx);
	setVal('SpaceIdx', ndx);
}

function Special_Anatomy(){
	var e = getElem('MastArt'),
		r = e.parentNode,
		path = $ENV['cdn']+'img/monero-outreach/specials/';

	console.log(e);

	setAttr(e, {'src':path+'anatomy_background.jpg', 'img-dna':''});

	r.innerHTML += '<div id="ALayer1">'+
			'<div class="ALeft"><div class="AInner">'+
				'<b>FUNGIBILITY:</b> Every privacy feature is to this end. Monero’s untraceability means you do not have to fear that coins you receive are tainted by association with the activity of their previous owners, just like with cash.<br><br>'+
				'<b>CONFIDENTIAL AMOUNTS:</b> Transaction amounts are behind encryption to prevent transactions from being traced by the amount sent.<br><br>'+
				'<b>HIGH SECURITY CRYPTOGRAPHY:</b> Cutting edge ED25519 elliptic curve cryptography that protects your funds from theft.<br><br>'+
				'<b>OPENALIAS IDENTITY:</b> A Monero project that makes paying someone else as easy as knowing their email address (no complicated strings of random numbers and letters required).'+
			'</div></div>'+
			'<div class="ACenter"><div class="CenterImg" style="background-image:url('+path+'anatomy_layer1.png)"></div></div>'+
			'<div class="ARight"><div class="AInner">'+
				'<b>ALWAYS-ON PRIVACY:</b> All transactions are confidential and every Monero user’s activity enhances the privacy of other users. Developers are even looking at how to maintain this feature under future quantum computing attack vectors.<br><br>'+
				'<b>UNTRACEABLE:</b> The source of funds for each transaction is concealed using ring signature cryptography. This is achieved without the need for a trusted setup, safeguarding against malicious currency fabrication.<br><br>'+
				'<b>NETWORK INVISIBILITY:</b> Monero has Tor and I2P integration to conceal network activity. It also uses Dandelion++, providing an additional layer of IP address obfuscation and keeping your identity safe.'+
			'</div></div>'+
		'</div>'+
		'<div id="ALayer2">'+
			'<div class="ALeft"><div class="AInner">'+
				'<b>DYNAMIC BLOCKSIZE:</b> Transaction volume is not restricted by an arbitrary cap, ensuring transactions are processed quickly and inexpensively, in a scalable way.<br><br>'+
				'<b>LIGHTING MEMORY MAPPED DATABASE (LMDB):</b> Built on an ultra high performance, low memory footprint database developed by a key Monero contributor who worked for NASA’s JPL as a software engineer supporting space shuttles.<br><br>'+
				'<b>FAST, EFFICIENT NODE NETWORK:</b> High performance, high scalability messaging protocol for node communication. Nodes can be also be incentivized with RPC-Pay.<br><br>'+
				'<b>PRUNABLE BLOCKCHAIN:</b> Transaction history on the Monero blockchain is prunable, helping improve scalability and reduce space requirements on user hardware.'+
			'</div></div>'+
			'<div class="ACenter"><div class="CenterImg" style="background-image:url('+path+'anatomy_layer2.png)"></div></div>'+
			'<div class="ARight"><div class="AInner">'+
				'<b>VIEW-KEY AUDITING:</b> Dual key system allows you to provide others with the ability to view your incoming funds without risk of theft. Privacy is on by default, but this feature lets you opt-out.<br><br>'+
				'<b>STEALTH ADDRESSES, SUBADDRESSES:</b> Your wallet address will never appear on the public blockchain and subaddresses let you create new accounts within the same wallet to add an even deeper layer of privacy. No one with your wallet address can see how rich (or poor) you are or how you spend.<br><br>'+
				'<b>ASIC RESISTANCE:</b> The innovative RandomX mining algorithm removes the ASIC advantage, allowing Monero to follow the egalitarian “1 CPU = 1 Vote” philosophy. This keeps mining accessible to everyone, reduces regional centralization, and is vastly more energy efficient than Bitcoin.<br><br>'+
				'<b>ATOMIC SWAPS:</b> Multiple teams are working on atomic swaps capabilities, allowing users to trade Bitcoin and Monero via protocol, outside the purview of any exchange. Expected by EOY 2021.'+
			'</div></div>'+
		'</div>'+
		'<div id="ALayer3">'+
			'<div class="ALeft"><div class="AInner">'+
				'<b>FRESH CODEBASE:</b> Written from the ground up to deliver genuine privacy and anonymity. Not just another Bitcoin or Ethereum clone.<br><br>'+
				'<b>ACCESSIBLE WALLETS:</b> Monero wallets are accessible via the web, mobile (Android, F-Droid, and iOS), and desktop. Popular hardware wallets also have integrated Monero, allowing users to keep their funds on a separate device.<br><br>'+
				'<b>GRASSROOTS COMMUNITY:</b> Over 540 developers have contributed to Monero code, with over 240 regular contributors. Decentralized, global project with over 20 workgroups who help build the ecosystem.<br><br>'+
				'<b>LOW FEES, FAST TRANSACTIONS:</b> Unlike Bitcoin which can cost several dollars to transact, you can make a Monero transaction for fractions of cents. Thanks to Bulletproofs+ these transactions are also fast and highly secured.'+
			'</div></div>'+
			'<div class="ACenter"><div class="CenterImg" style="background-image:url('+path+'anatomy_layer3.png)"></div></div>'+
			'<div class="ARight"><div class="AInner">'+
				'<b>DEV TALENT MAGNET:</b> Monero boasts the third largest developer count of all cryptocurrencies, led only by Bitcoin and Ethereum. True innovation attracts talent.<br><br>'+
				'<b>MONERO RESEARCH LAB:</b> Led by world class academic researchers in mathematics and cryptography developing new technologies for privacy and security. Developments are fully audited by third-parties before being added to the Monero protocol.<br><br>'+
				'<b>EXCHANGE TRADED:</b> Listed widely across multiple exchanges, including some of the largest like Kraken and Binance, allowing you to invest or move funds in and out of Monero easily.<br><br>'+
				'<b>VERIFIABLE INFLATION SUPPLY:</b> Monero supply can be verified by cross referencing multiple websites and a basic command in the official wallet. Supply is capped at ~18.5 million coins by May 2020, at which point tail emission will mine 0.6 XMR/block indefinitely to incentivize miners to secure the network and keep fees low.'+
			'</div></div>'+
		'</div>';

	Special_Anatomy_Resize();
	window.addEventListener('resize', Special_Anatomy_Resize);

	getElem('Layer1').oninput = function(){
		getElem('ALayer1').style.opacity = this.value / 100; 
	}
	getElem('Layer2').oninput = function(){
		getElem('ALayer2').style.opacity = this.value / 100; 
	}
	getElem('Layer3').oninput = function(){
		getElem('ALayer3').style.opacity = this.value / 100; 
	}
}
function Special_Anatomy_Resize(){
	var hd = getElem('header');
	if(VPortW < 600){
		hd.classList.remove('R16-9', 'R4-3');
		hd.classList.add('R1-1');
	}else if(VPortW < 800){
		hd.classList.remove('R16-9', 'R1-1');
		hd.classList.add('R4-3');
	}else{
		hd.classList.remove('R4-3', 'R1-1');
		hd.classList.add('R16-9');
	}
}