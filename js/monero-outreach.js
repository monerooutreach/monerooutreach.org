var MOresizeTimeout,
	Mshape = '<svg viewBox="0 0 365.1 23.1" preserveAspectRatio="xMidYMax meet"><path d="M365.1 23.1l-168-4.4V0l-14.5 14.5L168.1 0v18.7L0 23.1z"/></svg>',
	ico_newwindow = '<svg viewBox="0 0 99 99"><path d="M50 20L30 0h69v69L79 49c-16 7-30 18-36 50-17-32-11-58 7-79z"/></svg>',
	pgid = '',
	$MOFootIco = { '0':'Telegram', '1':'Twitter', '2':'Instagram', '3':'Reddit'},
	MOCookie = '';

function SpokeInitialized(){
	var lng = $PGE['lng'],
		nav_head = '',
		nav_burg = '',
		nav_foot = (lng === 'en') ? '<a href="https://www.monerooutreach.org/">Home</a>' : '',
		i = 0;

	load_LanguageLib(lng, 'app', function(){
		load_LanguageLib(lng, 'ui', function(){

			for(var k in $LNG['app'][lng]['nav']) {
				var v = $LNG['app'][lng]['nav'][k],
					url = '<a href="'+$ENV['host']+v['url']+'">'+v['lbl']+'</a>';
		
				nav_head += (i < 3) ? url+' &nbsp; ' : url;
				nav_burg += '<li>'+url+'</li>';
				nav_foot += url;
				i++;
			}
		
			getElem('Mast').innerHTML += '<div id="MastLangSelect" class="hide"></div><div id="MastMenuSelect" class="hide"></div>';
			getElem('MastMenu').innerHTML = '<span class="MenuS">'+nav_head+'</span>';
			getElem('MastBurger').querySelector('li ul').innerHTML = nav_burg;
			getElem('FootLeft').innerHTML = nav_foot;
			
			var Flex0 = ($PGE && $PGE['idx0']) ? $PGE['idx0'] : '',
				hd = getElem('header');
				
			if(pgid === 'home'){
				hd.classList.add('R2-1');
				init_Home_v2();
			}else if(Flex0 === 'Calendar'){
				hd.classList.add('R4-1');
				load_Scripts(['js/lib/Calendar', 'js/apps/monero-outreach-calendar'], function(){
					Calendar_init($PGE['loc'], $PGE['lng'], function(){
						MOCalendar();
					});
				});
			}else{
				if(pgid === 'sitemap'){
					var sm = '',
						s = getElem('Sitemap');
		
					get_Data('query_Content', 'Content', {'loc':$PGE['loc'], 'lng':$PGE['lng'], 'typ':'F', 'lim':'99', 'ocol':'title'}, function(d){
						if(d && d['data']){
							$.each(d['data'],function(k, v){
								var $U = prepURL(v);
								sm += '<p><a href="'+$U['url']+'">'+v['title']+'</a><ul id="'+v['folder']+'"></ul></p>';
								get_Data('query_Content', 'Content', {'loc':$PGE['loc'], 'lng':$PGE['lng'], 'fld':v['folder'], 'typ':'P', 'lim':'99'}, function(p){
									if(p && p['data']){
										var pm = '';
										$.each(p['data'],function(kp, vp){
											if(vp['flex'] && vp['flex']['redir_tar']){
		
											}else{
												var $U = prepURL(vp);
												pm += '<li><a href="'+$U['url']+'">'+vp['title']+'</a></li>';
											}
										});
										$('#'+v['folder']).html(pm);
									}
								});
							});
							s.innerHTML = sm;
						}else{
							s.innerHTML = '<p>No Results</p>';
						}
					});
				}else if(pgid === 'stories'){
					ContentList('stories', '1');
					if(!$PGE['pagin'] || $PGE['pagin'] == '1'){
						get_Data('query_Content', 'Content', {'loc':$PGE['loc'], 'lng':$PGE['lng'], 'typ':'P', 'flt1':'idx1@HomeFeature', 'lim':'1'}, function(d){
							if(d && d['data'] && d['data'][0]){
								d = d['data'][0];
								var $U = prepURL(d),
									byline = (d['byline']) ? d['byline']+' &#124; ' : '';
								
								getElem('stories__topstory').innerHTML = '<div class="Col2"><div class="LR95">'+MOThumb(d['thumb'], $U)+'</div></div>'+
									'<div class="Col2 middle">'+
										'<h1><a '+$U['a']['full']+'><span class="C3">'+d['title']+'</span></a></h1>'+
										'<h2>'+$LNG['ui'][$PGE['lng']]['txt']['lq']+d['caption']+$LNG['ui'][$PGE['lng']]['txt']['rq']+' </h2>'+
										'<div class="txt_copytext shimtop10">'+byline+prepDate(d['pub_start'], 'dte_long')+' | <a '+$U['a']['full']+'><i class="C3">Read More</i></a></div>'+
									'</div>';
									
								Sizer('stories__topstory');
							}
						});
					}
				}else if(pgid === 'video'){
					ContentList('video', '1');
				}else if(pgid === 'gallery'){
					ContentList('gallery', '1');
				}else if(pgid === 'monero-in-the-wild'){
					ContentList('monero-in-the-wild', '1');
				}else if(pgid === 'monero-infographic'){
					ContentList('monero-infographic', '1');
				}else if(pgid === 'monero-says'){
					ContentList('monero-says', '1');
				}else if(pgid === 'PSA'){
					ContentList('PSA', '1');
				}else if(pgid === 'news-room'){
					ContentList(pgid, '1');
				}else if(pgid === 'BreakingMonero'){
					ContentList('BreakingMonero', '1');
				}else if(pgid === 'Konferenco'){
					ContentList('Konferenco1', '1');
					ContentList('Konferenco2', '1');
				}else if(pgid === 'Resources'){
					ContentList('ResourceTopics', '1');
				}
				byClass('aboutarrowpoint', 'app', '<svg viewBox="0 0 44 88"><path d="M44 44L0 0v88z"/></svg>');
				byClass('aboutarrowtail', 'app', '<svg viewBox="0 0 102 119"><path d="M102 51V0H83C26 0-21 56 10 119c14-51 43-68 92-68z"/></svg>');
			}
		
			byClass('bcthome', 'ins', $ICO['home']);
			MOMachineTranslated();
			MOLicense();
		
			var dh = getElem('HeadDate');
			if(dh){
				var mde = (dh.getAttribute('data-mde') === 'L') ? 'L' : 'R',
					ins = '<table class="StoryDate TDPadS txt_copytext"><tr>';
		
				if(mde === 'R'){
					ins += 	'<td width="99%"></td><td NOWRAP>'+prepDate($PGE['pub_start'], 'dte_long')+'</td>';
				}
				
				if($PGE['flex']){
					if($PGE['flex']['reddit']){
						ins += '<td><a href="'+$PGE['flex']['reddit']+'" class="StoryOnReddit Btn32 C1bk C2bk_hov C0fl" target="_blank"><div class="Sprite" style="background-position:-66px 0"></div></a></td>';
					}
					if($PGE['flex']['twitter']){
						ins += '<td><div class="StoryOnTwitter NewWindow Btn32 C1bk C2bk_hov C0fl" data-url="'+$PGE['flex']['reddit']+'"><div class="Sprite" style="background-position:-22px 0"></div></div></td>';
					}
					if($PGE['flex']['instagram']){
						ins += '<td><div class="StoryOnInstagram NewWindow Btn32 C1bk C2bk_hov C0fl" data-url="'+$PGE['flex']['reddit']+'"><div class="Sprite" style="background-position:-66px 0"></div></div></td>';
					}
				}
		
				if(mde === 'L'){
					ins += 	'<td NOWRAP>'+prepDate($PGE['pub_start'], 'dte_long')+'</td><td width="99%"></td>';
				}
		
				ins += '</tr></table>';
				dh.innerHTML = ins;
			}else{
				byClass('StoryOnReddit', 'ins', '<div class="Sprite" style="background-position:-66px 0"></div>');
				byClass('StoryOnTwitter', 'ins', '<div class="Sprite" style="background-position:-22px 0"></div>');
			}
			
			for(var k in $MOFootIco){
				getElem('Foot'+$MOFootIco[k]).innerHTML = '<div class="Sprite" style="background-image:url('+$ENV['cdn']+'img/monero-outreach/svg/footer_sprite_min.svg);background-position:-'+(k * 20)+'px 0"></div>';
			}
		
			MOResize();
		});
	});
}

function MastLangOpen(e){
	var s = getElem('MastLangSelect');
	e.innerHTML = '<div class="spin">'+$ICO['spoke']+'</div>';
	if(s.classList.contains('hide')){
		s.classList.remove('hide');
		load_LanguageLib('', 'sys', function(){
			get_Data('query_Translations', 'Content', {'xid':$PGE['xid'], 'lnglnk':$PGE['lnglnk']}, function(d){
				if(d && d['data'] && isObj(d['data']) > 0){
					var ins = '';
					for(var k in d['data']){
						var v = d['data'][k];
						if(v['lng'] !== $PGE['lng']){
							var $U = prepURL(v),
								lbl = (v['lng'] in $LNG['sys']) ? $LNG['sys'][v['lng']] : v['loc']+'-'+v['lng'],
								url = ($U['url'] === 'https://www.monerooutreach.org/index.html') ? 'https://www.monerooutreach.org/' : $U['url']; //hack
								
							ins += '<a href="'+url+'">'+lbl+'</a>';	
						}
					}
					s.innerHTML = ins;
					e.innerHTML = '<div class="rot45">'+$ICO['plus']+'</div>';
				}
			});
		});
	}else{
		e.innerHTML = $ICO['translate'];
		s.classList.add('hide');
	}
}

function MastMenuOpen(e){
	var s = getElem('MastMenuSelect');
	e.innerHTML = '<div class="spin">'+$ICO['spoke']+'</div>';
	if(s.classList.contains('hide')){
		s.classList.remove('hide');
		e.innerHTML = '<div class="rot45">'+$ICO['plus']+'</div>';
		s.innerHTML = '<div data-toggle="cookies"><span>Cookies Off</span><div id="CookieSwitch" class="Switch">'+$ICO['switch']+'</div></div>'+
			'<div data-toggle="appmode"><span>App Mode</span><div id="AppModeSwitch" class="Switch">'+$ICO['switch']+'</div></div>';

		MOCookie = getCookie('');
		if(MOCookie) Switch('T', 'CookieSwitch');
		if($ENV['mode'] === 'app') Switch('T', 'AppModeSwitch');
	}else{
		s.classList.add('hide');
		e.innerHTML = $ICO['gear'];
	}
}

function init_Home_v2(){
	var m = getElem('main');
	m.innerHTML = '<div id="LeadStory" class="LR80 shim20"></div><div class="hbar shimtop5 LR95"></div>'+m.innerHTML;

	FeatureList('LeadStory');
	ContentList('stories', '1');
	ContentList('video', '1');
	ContentList('gallery', '1');
}

function ContentList(tar, pagpge){
	var ins = '',
		e = getElem(tar+'__list'),
		c = 0,
		dsply = 'grid';

	if(e){
		if($PGE['pagin']) pagpge = $PGE['pagin'];
		
		var $Q = {
			'loc':$PGE['loc'],
			'lng':$PGE['lng'],
			'lim':12,
			'pge':pagpge,
			'ocol':'pub_start',
			'odir':'D',
			'paginate':'y'
		};
		
		e.innerHTML = '<div class="center shim30"><div class="Ico36"><div class="C3fl spin">'+$ICO['spoke']+'</div></div></div>';
	
		if(tar === 'gallery'){
			$Q['flt1'] = 'idx0@Gallery';
		}else if(tar === 'monero-in-the-wild'){
			$Q['flt1'] = 'idx1@Monero in the Wild';
		}else if(tar === 'monero-infographic'){
			$Q['flt1'] = 'idx1@Infographic';
		}else if(tar === 'monero-says'){
			$Q['flt1'] = 'idx1@Monero Says';
			$Q['mde'] = 'adm';
		}else if(tar === 'video'){
			$Q['flt1'] = 'idx0@Videos';
		}else if(tar === 'stories'){
			$Q['flt1'] = 'idx0@Stories';
		}else if(tar === 'news-room'){
			$Q['flt1'] = 'idx0@News';
		}else if(tar === 'merchantcampaign'){
			$Q['flt1'] = 'idx1@MerchantCampaign';
			$Q['paginate'] = 'n';
		}else if(tar === 'PSA'){	
			$Q['flt1'] = 'idx0@PSA';
			$Q['typ'] = 'p';
			$Q['paginate'] = 'n';
			$Q['ocol'] = 'idx1';
			$Q['lim'] = 20;
		}else if(tar === 'BreakingMonero'){
			dsply = 'list';
			$Q['flt1'] = 'idx0@BreakingMonero';
			$Q['paginate'] = 'n';
			$Q['odir'] = 'A';
			$Q['lim'] = 20;
		}else if(tar === 'Konferenco1' || tar === 'Konferenco2'){
			dsply = 'list';
			$Q['flt1'] = 'idx0@Konferenco';
			$Q['paginate'] = 'n';
			$Q['odir'] = 'A';
			$Q['lim'] = 20;
			if(tar === 'Konferenco1'){
				$Q['flt2'] = 'idx1@Day1';
			}else{
				$Q['flt2'] = 'idx1@Day2';
			}
		}else if(tar === 'ResourceTopics'){
			$Q['flt1'] = 'idx0@Resources';
			$Q['mde'] = 'adm';
		}
		
		if(pgid === 'home'){
			$Q['lim'] = 4;
			$Q['paginate'] = 'n';
		}
	
		get_Data('query_Content', 'Content', $Q, function(d){
			if(d && d['data']){
				for(var k in d['data']){
					if(dsply === 'grid' && (c === 0 || c === 2)) ins += '<div class="Magic900 Col2">';
					var v = d['data'][k],
						$U = prepURL(v),
						idna = ($U['div']['classname'] === 'LinkPop' && v['thumb']) ? ' img-dna="'+v['thumb']+'"' : '',
						caption = (v['caption']) ? prepHTML(v['caption'], v) : '',
						storydate = (pgid === 'news-room') ? prepDate(v['pub_start'], 'dte')+' - ' : '';
					
					if(dsply === 'grid'){
						ins += '<div class="Magic500 Col2 shim5">'+
							'<div class="articletile LR98">'+MOThumb(v['thumb'], $U)+'<a '+$U['a']['full']+idna+'><h2 class="article_headline zerotop">'+v['title']+'</h2></a>'+
								'<p class="article_caption zerotop">'+storydate+caption+'</p>'+
							'</div>'+
						'</div>';	
					}else{
						var $THM = prepIMG(v['thumb']);
						ins += '<div class="MOListText">'+
							'<h2>'+v['title']+'</h2>'+
							'<p><a '+$U['a']['full']+idna+'>'+caption+'</a></p>'+
							'<div class="MOListThumb Sizer" style="background-image:url(##IMG##'+$THM['min']+')" img-dna="'+v['thumb']+'"></div>'+
						'</div>';
					}
		
					if(dsply === 'grid' && (c === 1 || c === 3)) ins += '</div>';
					if(c === 3){
						c = 0;
					}else{
						c++
					}
				}
	
				e.innerHTML = ins;
				Sizer(tar+'__list');
	
				var pagelem = getElem(tar+'__pagination');
				if(pagelem){
					setAttr(pagelem, {'data-fnc':'LoadSpoke_PgV4_SetPage', 'data-pge':pagpge});
	
					Pagination_init(tar+'__pagination', pagpge, $Q['lim'], d['tot']);
					Pagination_init(tar+'__pagination__bottom', pagpge, $Q['lim'], d['tot']);
				}
			}else{
				ins += '<p>No Results.</p>';
			}
		});
	}
}

function gallery__pagination_SetPage(pge){
	ContentList('gallery', pge);
}

function video__pagination_SetPage(pge){
	ContentList('video', pge);
}

function stories__pagination_SetPage(pge){
	ContentList('stories', pge);
}

function FeatureList(tar){
	var e = getElem(tar);
	if(e){
		get_Data('query_Content', 'Content', {'loc':$PGE['loc'], 'lng':$PGE['lng'], 'typ':'P', 'flt1':'idx1@HomeFeature', 'lim':'1'}, function(d){	
			if(d && d['data']){
				d = d['data'][0];
				var ls = getElem('LeadStory'),
					$U = prepURL(d),
					byline = (d['byline']) ? ' &#124; '+d['byline'] : '',
					thm = prepIMG(d['thumb']);
				
				getElem('LeadHeadline').innerHTML = '<h1><a '+$U['a']['full']+'><span class="C3">'+d['title']+'</span></a></h1>';
				ls.innerHTML = '<h2>'+$LNG['ui'][$PGE['lng']]['txt']['lq']+d['caption']+$LNG['ui'][$PGE['lng']]['txt']['rq']+'<span class="txt_copytext shim5"> '+prepDate(d['pub_start'], 'dte_long')+byline+' &#124; <a '+$U['a']['full']+'><i class="C3">Read the Story</i></a></span></h2>';
				ls.classList.remove('hide');
				ls.style.display = 'block';
				
				if(thm){
					setAttr(getElem('MastArt'), {'src':$ENV['cdn']+thm['min'], 'img-dna':d['thumb'], 'alt':d['title']});
					Sizer('header');
				}
			}
		});
	}
}

function MOResize(){
	clearTimeout(MOresizeTimeout);
	MOresizeTimeout = setTimeout(function(){
		var mm = getElem('MastMenu'),
			ms = mm.getElementsByTagName('span')[0],
			w = 0;

		if(mm){
			w = mm.clientWidth;
		}

		if(ms.clientWidth >= (w * .97)){
			ms.style.top = '-100px';
			getElem('MastBurger').classList.remove('hide');
		}else{
			ms.style.top = '0';
			getElem('MastBurger').classList.add('hide');
		}
	}, 250);
}
function MOMachineTranslated(){
	var e = getElem('MachineTranslated');
	if(e){
		e.setAttribute('class', 'txt_copytext');
		e.innerHTML = '<i>'+$LNG['app'][FMLng]['machine']+'</i>';	
	}
}
function MOLicense(){
	var e = getElem('MOLicense');
	if(e){
		if($PGE && $PGE['flex'] && $PGE['flex']['license'] && $PGE['flex']['license'] != ''){
			var lic = $PGE['flex']['license'];
			if(isObj($LNG['app'][FMLng]['licences']) > 0 && lic in $LNG['app'][FMLng]['licences']){
				e.innerHTML = '<div class="Ico24 C3fl"><svg viewBox="0 0 99 99"><path d="M77.5 41.5L71 44.9a6.7 6.7 0 00-5.5-4c-4.4 0-6.6 3-6.6 8.8 0 4.5 1.8 8.7 6.6 8.7 2.9 0 5-1.4 6.1-4.2l6.1 3a15 15 0 01-13 7.9c-10.9-.2-14.7-7.6-14.8-15.4.2-9.5 5.6-15.2 14.5-15.4 6.2 0 10.6 2.4 13.2 7.2zm-28.4 0l-6.6 3.4a6.7 6.7 0 00-5.5-4c-4.4 0-6.6 3-6.6 8.8 0 4.5 1.8 8.7 6.6 8.7 2.9 0 5-1.4 6-4.2l6.2 3a15 15 0 01-13 7.9c-10.9-.2-14.7-7.6-14.8-15.4.2-9.5 5.6-15.2 14.5-15.4 6.2 0 10.5 2.4 13.2 7.2zM49.5 0A49.5 49.5 0 000 49.5 49.5 49.5 0 0049.5 99 49.5 49.5 0 0099 49.5 49.5 49.5 0 0049.5 0zm0 9A40.5 40.5 0 0190 49.5 40.5 40.5 0 0149.5 90 40.5 40.5 0 019 49.5 40.5 40.5 0 0149.5 9z"/></svg></div>'+
					'<div class="txt_smalltext">'+$LNG['app'][FMLng]['licences']['lbl']+' <span class="NewWindow C1_link" data-url="'+$LNG['app'][FMLng]['licences'][lic]['url']+'">'+$LNG['app'][FMLng]['licences'][lic]['nme']+'</span></div>';
			}
			
		}
	}
}
function MOContribute(re){
	OpenModal('450','450');
	getElem('ModalBox').innerHTML += '<div class="LR95 fullH">'+
		'<div id="ContributeHeader" class="shimtop20 txt_subhead">'+$LNG['app'][FMLng]['contact']['lbl']+'</div>'+
		'<div id="ContributeText"></div>'+
		'<input type="text" id="ContributeSubject" class="shimtop5" placeholder="Subject..." />'+
		'<textarea id="ContributeMessage" rows="9" class="shimtop5" placeholder="'+$LNG['app'][FMLng]['contact']['disclaim']+'" style="height:calc(100% - 160px)"></textarea>'+
		'<div id="MOContributeBtn" class="shimtop10 BtnSubmit MenuM">SEND</div>'+
	'</div>';
	
	if($PGE){
		var $U = prepURL($PGE);
		if($U['url']) getElem('ContributeSubject').value = 're: '+$U['url'].replace('https://www.', '');
	}
}

function MOThumb(thumbdna, $U){
	var ins = '';
	if(thumbdna){
		var $THM = prepIMG(thumbdna);
		if($THM){
			var thumbclr = ($THM['color']) ? $THM['color'] : '#eaeaea',
				newwindow = ($U['mde'] === 'blank') ? '<div class="article_newwindow">'+ico_newwindow+'</div>' : '',
				idna = ($U['div']['classname'] === 'LinkPop') ? ' img-dna="'+thumbdna+'"' : '';
				
			ins = '<a '+$U['a']['full']+idna+'>'+
				'<div class="article_thumbnail Sizer" style="background-color:'+thumbclr+'" img-dna="'+thumbdna+'">'+
					'<div class="article_thumbnailscreen"><div class="article_thumbnailbottom">'+Mshape+'</div></div>'+newwindow+
				'</div>'+
			'</a>';
		}
	}
	return ins;
}

function init_FlowChart(){
	var e = document.querySelectorAll('.FlowC_Arrow');
	for(var i = 0; i < e.length; i++){
		var dir = e[i].getAttribute('data-dir'),
			elClass = '',
			dirClass = '';

		if(dir === 'L'){
			dirClass = 'rot180';
		}else if(dir === 'U'){
			dirClass = 'rot270';
		}else if(dir === 'D'){
			dirClass = 'rot90';
		}else if(dir === 'UL'){
			dirClass = 'rot225';
			elClass = 'FlowC_DiagUp';
		}else if(dir === 'UR'){
			dirClass = 'rot315';
			elClass = 'FlowC_DiagUp';
		}else if(dir === 'DL'){
			dirClass = 'rot135';
			elClass = 'FlowC_DiagDown';
		}else if(dir === 'DR'){
			dirClass = 'rot45';
			elClass = 'FlowC_DiagDown';
		}

		if(elClass) e[i].classList.add(elClass);
		e[i].innerHTML = '<div class="FlowC_ArrowInner '+dirClass+'">'+
				'<div class="FlowC_Shaft"></div>'+
				'<div class="FlowC_Head"><svg viewBox="0 0 9 9"><path d="M0 0l9 4.5L0 9z"/></svg></div>'+
			'</div>';
	}
}

/*! MO Events */
window.addEventListener('resize', MOResize);

document.body.addEventListener('pointerup', f => {
	var e = f.target,
		id = e.getAttribute('id');

	if(id === 'MOContributeBtn'){
		getElem('MOContributeBtn').innerHTML = 'Sending...';
		var $D = {
				'loc':$PGE['loc'],
				'lng':$PGE['lng'],
				'message':getVal('ContributeSubject')+'\n\n'+getVal('ContributeMessage')
			};

		get_Data('manage_Message', '', $D, function(p){
			if(p && p['msg'] === 'OK'){
				getElem('ContributeHeader').innerHTML = 'Thank You';
				getElem('ContributeText').innerHTML = '<div class="txt_copytext center shimtop30">Message has been sent.</div>';
				RemoveDiv('ContributeSubject');
				RemoveDiv('ContributeMessage');
				RemoveDiv('MOContributeBtn');
			}else{
				Error('Message', p);
			}
		});
	}else if(id === 'MastLang'){
		MastLangOpen(e);
	}else if(id === 'MastConnect'){
		MastMenuOpen(e);
	}else{
		var c = e.classList;
		if(c.contains('MOContribute')){
			MOContribute();
		}
	}
});